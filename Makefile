.PHONY: build
build:
	CGO_ENABLED=0 go build -o bin/app -mod=readonly

.PHONY: test
test:
	go test -v ./... -race -cover

.PHONY: lint
lint:
	golangci-lint run

.PHONY: run
run:
	go run ./cmd/app/main.go

.PHONY: migrup
migrup:
	go run ./cmd/migration/migrator.go up

.PHONY: migrdown
migrdown:
	go run ./cmd/migration/migrator.go down
