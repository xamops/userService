package di

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

func init() {
	if err := godotenv.Load("./config/.env"); err != nil {
		log.Panicf("failed to load .env file: %s", err.Error())
	}
}

func MustEnv(name string) string {
	if os.Getenv(name) == "" {
		log.Panicf("unknown parameter: %s", name)
	}
	return os.Getenv(name)
}

func Env(name string, defaultValue string) string {
	if os.Getenv(name) == "" {
		return defaultValue
	}
	return os.Getenv(name)
}
