package di

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"strconv"
)

var db *sql.DB

func Database() *sql.DB {
	if db != nil {
		return db
	}

	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		MustEnv("DB_HOST"), MustEnv("DB_PORT"),
		MustEnv("DB_USER"), MustEnv("DB_PASSWORD"),
		MustEnv("DB_DATABASE"), MustEnv("DB_SSL"))

	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("failed to create a new postgres (v2) connection: %s", err.Error())
		return nil
	}

	idleConn, err := strconv.Atoi(Env("DB_MAX_IDLE_CONNS", "1"))
	if err != nil {
		log.Panicf("parse db max idle conn: %s", err.Error())
		return nil
	}

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	db.SetMaxIdleConns(idleConn)

	openConn, err := strconv.Atoi(Env("DB_MAX_CONNS", "1"))
	if err != nil {
		log.Panicf("parse db max conn: %s", err.Error())
		return nil
	}

	db.SetMaxOpenConns(openConn)

	if err = db.Ping(); err != nil {
		log.Fatalf("failed to ping to database: %s", err.Error())
		return nil
	}

	return db
}
