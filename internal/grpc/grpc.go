package grpc

import (
	"context"
	userservicev1 "gitlab.com/xamops/proto/userservice/gen/go"
	"google.golang.org/grpc"
)

type serverAPI struct {
	userservicev1.UnimplementedUsersServer
}

func Register(gRPC *grpc.Server) {
	userservicev1.RegisterUsersServer(gRPC, &serverAPI{})
}

func (s *serverAPI) Create(ctx context.Context, in *userservicev1.CreateUserRequest) (*userservicev1.CreateUserResponse, error) {
	panic("implement me")
}

func (s *serverAPI) Get(ctx context.Context, in *userservicev1.GetUserRequest) (*userservicev1.GetUserResponse, error) {
	panic("implement me")
}

func (s *serverAPI) List(ctx context.Context, in *userservicev1.ListUsersRequest) (*userservicev1.ListUsersResponse, error) {
	panic("implement me")
}

func (s *serverAPI) Update(ctx context.Context, in *userservicev1.UpdateUserRequest) (*userservicev1.UpdateUserResponse, error) {
	panic("implement me")
}

func (s *serverAPI) Delete(ctx context.Context, in *userservicev1.DeleteUserRequest) (*userservicev1.DeleteUserResponse, error) {
	panic("implement me")
}
