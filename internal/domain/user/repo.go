package user

import (
	"context"
	"time"
)

type Repository interface {
	FindOne(ctx context.Context, id string) (*User, error)
	Find(ctx context.Context, cond *Cond, limit, offset int) ([]*User, error)
	Create(ctx context.Context, user *User) error
	Update(ctx context.Context, user *User) error
	Delete(ctx context.Context, id string) error
}

type Cond struct {
	IDs           []string
	Logins        []string
	Statuses      []UserStatus
	Roles         []UserRole
	Emails        []string
	Descriptions  []string
	AuthIDs       []string
	CreatedAtFrom *time.Time
	CreatedAtTo   *time.Time
}
