package mock

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/xamops/userService/internal/domain/user"
)

type MockUserRepo struct {
	mock.Mock
}

func (m *MockUserRepo) FindOne(ctx context.Context, id string) (*user.User, error) {
	args := m.Called(id)
	if args.Get(1) != nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*user.User), nil
}

func (m *MockUserRepo) Find(ctx context.Context, cond *user.Cond, limit, offset int) ([]*user.User, error) {
	args := m.Called(cond)
	if args.Get(1) != nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]*user.User), nil
}

func (m *MockUserRepo) Create(ctx context.Context, usr *user.User) error {
	args := m.Called(usr)
	return args.Error(0)
}

func (m *MockUserRepo) Update(ctx context.Context, usr *user.User) error {
	args := m.Called(usr)
	return args.Error(0)
}

func (m *MockUserRepo) Delete(ctx context.Context, id string) error {
	args := m.Called(id)
	return args.Error(0)
}
