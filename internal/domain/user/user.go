package user

import (
	"errors"
	"github.com/xamust/xvalidator"
	"gitlab.com/xamops/userService/internal/app/metadata"
	"gitlab.com/xamops/userService/internal/service/validator"
	"strings"
	"time"
)

var (
	ErrUserEmptyLogin    = errors.New("login can't be empty")
	ErrUserEmptyName     = errors.New("name can't be empty")
	ErrUserUnknownStatus = errors.New("unknown user status")
	ErrUserUnknownRole   = errors.New("unknown user role")
	ErrUserEmptyID       = errors.New("user id can't be empty")
	ErrUserEmptyAuthID   = errors.New("auth id can't be empty")
)

type (
	UserStatus string
	UserRole   string
)

const (
	Inactive UserStatus = "inactive"
	Active   UserStatus = "active"
	Blocked  UserStatus = "blocked"

	Admin UserRole = "admin"
	Usr   UserRole = "user"
	Owner UserRole = "owner"
)

type User struct {
	id          string
	login       string
	isActive    bool
	status      UserStatus
	name        string
	email       string
	description string
	role        UserRole
	authID      string
	metadata    metadata.Metadata
	createdAt   time.Time
	updatedAt   time.Time
	deletedAt   time.Time
}

func (usr *User) ID() string                  { return usr.id }
func (usr *User) AuthID() string              { return usr.authID }
func (usr *User) Login() string               { return usr.login }
func (usr *User) Status() UserStatus          { return usr.status }
func (usr *User) IsActive() bool              { return usr.isActive }
func (usr *User) Name() string                { return usr.name }
func (usr *User) Email() string               { return usr.email }
func (usr *User) Description() string         { return usr.description }
func (usr *User) Role() UserRole              { return usr.role }
func (usr *User) Metadata() metadata.Metadata { return usr.metadata }
func (usr *User) CreatedAt() time.Time        { return usr.createdAt }
func (usr *User) UpdatedAt() time.Time        { return usr.updatedAt }
func (usr *User) DeletedAt() time.Time        { return usr.deletedAt }

type CreateInput struct {
	AuthID      string
	Login       string
	Status      UserStatus
	Name        string
	Email       string
	Description string
	Role        UserRole
	Metadata    metadata.Metadata
}

func (c *CreateInput) Validate() error {
	val, err := validator.NewValidator()
	if err != nil {
		return err
	}
	c.AuthID = strings.TrimSpace(c.AuthID)
	if c.AuthID == "" {
		return ErrUserEmptyAuthID
	}
	c.Login = strings.TrimSpace(c.Login)
	if c.Login == "" {
		return ErrUserEmptyLogin
	}
	if c.Status == "" {
		c.Status = Inactive
	}
	if c.Status != Inactive && c.Status != Active && c.Status != Blocked {
		return ErrUserUnknownStatus
	}
	c.Name = strings.TrimSpace(c.Name)
	if c.Name == "" {
		return ErrUserEmptyName
	}
	c.Description = strings.TrimSpace(c.Description)
	if c.Role != Admin && c.Role != Usr && c.Role != Owner {
		return ErrUserUnknownRole
	}
	if err := val.Validate(xvalidator.InputValData{Key: "email", ValData: c.Email}); err != nil {
		return err
	}
	return nil
}

func (c *CreateInput) New() (*User, error) {
	if err := c.Validate(); err != nil {
		return nil, err
	}
	return &User{
		authID:      c.AuthID,
		login:       c.Login,
		isActive:    true,
		status:      c.Status,
		name:        c.Name,
		email:       c.Email,
		description: c.Description,
		role:        c.Role,
		metadata:    c.Metadata,
	}, nil
}

type UpdateInput struct {
	ID          string
	AuthID      string
	Login       string
	Status      UserStatus
	Name        string
	Email       string
	Description string
	Role        UserRole
	Metadata    metadata.Metadata
}

func (c *UpdateInput) Validate() error {
	val, err := validator.NewValidator()
	if err != nil {
		return err
	}
	c.ID = strings.TrimSpace(c.ID)
	if c.ID == "" {
		return ErrUserEmptyID
	}
	c.AuthID = strings.TrimSpace(c.AuthID)
	if c.AuthID == "" {
		return ErrUserEmptyAuthID
	}
	c.Login = strings.TrimSpace(c.Login)
	if c.Login == "" {
		return ErrUserEmptyLogin
	}
	if c.Status == "" {
		c.Status = Inactive
	}
	if c.Status != Inactive && c.Status != Active && c.Status != Blocked {
		return ErrUserUnknownStatus
	}
	c.Name = strings.TrimSpace(c.Name)
	if c.Name == "" {
		return ErrUserEmptyName
	}
	c.Description = strings.TrimSpace(c.Description)
	if c.Role != Admin && c.Role != Usr && c.Role != Owner {
		return ErrUserUnknownRole
	}
	if err := val.Validate(xvalidator.InputValData{Key: "email", ValData: c.Email}); err != nil {
		return err
	}
	return nil
}

func (usr *User) Update(in *UpdateInput) error {
	if err := in.Validate(); err != nil {
		return err
	}
	if in.ID != usr.id && in.ID != "" {
		usr.id = in.ID
	}
	if in.AuthID != usr.authID && in.AuthID != "" {
		usr.authID = in.AuthID
	}
	if in.Login != usr.login && in.Login != "" {
		usr.login = in.Login
	}
	if in.Status != usr.status {
		usr.status = in.Status
	}
	if in.Name != usr.name && in.Name != "" {
		usr.name = in.Name
	}
	if in.Email != usr.email {
		usr.email = in.Email
	}
	if in.Description != usr.description && in.Description != "" {
		usr.description = in.Description
	}
	if in.Role != usr.role {
		usr.role = in.Role
	}

	// metadata will be override!!!!
	usr.metadata = in.Metadata

	return nil
}

func (usr *User) Delete() {
	usr.deletedAt = time.Now()
	usr.isActive = false
}

func (usr *User) MakeAdmin() {
	usr.role = Admin
}

func (usr *User) MakeUser() {
	usr.role = Usr
}

func (usr *User) SetActiveStatus() {
	usr.status = Active
}

func (usr *User) SetBlockedStatus() {
	usr.status = Blocked
}

func (usr *User) SetInactiveStatus() {
	usr.status = Inactive
}
