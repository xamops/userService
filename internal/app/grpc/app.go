package grpc

import (
	"fmt"
	usrserviceGRPC "gitlab.com/xamops/userService/internal/grpc"
	"gitlab.com/xamops/userService/internal/service/logger"
	"google.golang.org/grpc"
	"log/slog"
	"net"
)

const (
	runOp = "grpc.Run"
)

type App struct {
	log     *slog.Logger
	gRPCSrv *grpc.Server
	port    int
}

func NewApp(log *slog.Logger, port int) *App {
	gRPCSrv := grpc.NewServer()
	usrserviceGRPC.Register(gRPCSrv)
	return &App{
		log:     log,
		gRPCSrv: gRPCSrv,
		port:    port,
	}
}

func (a *App) Run() error {
	log := a.log.With(
		logger.AddOperation(runOp),
		slog.Int("port", a.port),
	)

	log.Info("starting gRPC server...")

	listen, err := net.Listen("tcp", fmt.Sprintf(":%d", a.port))
	if err != nil {
		return fmt.Errorf("failed to start gRPC server: %w", err)
	}

	log.Info("gRPC server is running", slog.String("addr", listen.Addr().String()))
	return nil
}
