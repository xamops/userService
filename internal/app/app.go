package app

import (
	grpcApp "gitlab.com/xamops/userService/internal/app/grpc"
	"log/slog"
	"time"
)

type App struct {
	GRPCSrv *grpcApp.App
}

func NewApp(log *slog.Logger,
	grpcPort int,
	storage string,
	ttl time.Duration) *App {
	// init db

	// init userServiceApp

	// init grpcApp
	grpcApp.NewApp()
	return App{GRPCSrv: }
}
