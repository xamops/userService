package metadata

import (
	"encoding/json"
	"strings"
)

type Metadata map[string]interface{}

func New() Metadata {
	return Metadata{}
}

func (md Metadata) Bytes() ([]byte, error) {
	if md == nil {
		return json.Marshal(New())
	}
	return json.Marshal(md)
}

func Load(b []byte) (Metadata, error) {
	if len(b) == 0 {
		return New(), nil
	}
	md := New()
	if err := json.Unmarshal(b, &md); err != nil {
		return nil, err
	}
	return md, nil
}

func (md Metadata) Add(key string, val interface{}) {
	md[prepareKey(key)] = val
}

func (md Metadata) Get(key string) interface{} {
	if val, exists := md[prepareKey(key)]; exists {
		return val
	}
	return nil
}

var prepareKey = func(s string) string {
	return strings.TrimSpace(strings.ToLower(s))
}
