package usecases

import (
	"context"
	"gitlab.com/xamops/userService/internal/domain/user"
)

type UserCreator interface {
	Create(ctx context.Context, user *user.CreateInput) (*user.User, error)
}

type userCreator struct {
	userRepo user.Repository
}

func NewUserCreator(userRepo user.Repository) UserCreator {
	return &userCreator{
		userRepo: userRepo,
	}
}

func (uc *userCreator) Create(ctx context.Context, usr *user.CreateInput) (*user.User, error) {
	u, err := usr.New()
	if err != nil {
		return nil, err
	}
	if err := uc.userRepo.Create(ctx, u); err != nil {
		return nil, err
	}
	return u, nil
}
