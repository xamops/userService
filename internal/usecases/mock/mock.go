package mock

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/xamops/userService/internal/domain/user"
)

type MockUsecases struct {
	mock.Mock
}

func (m *MockUsecases) Create(ctx context.Context, in *user.CreateInput) (*user.User, error) {
	args := m.Called(in)
	if args.Get(1) != nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*user.User), nil
}

func (m *MockUsecases) Delete(ctx context.Context, userID string) (string, error) {
	args := m.Called(userID)
	if args.Get(1) != nil {
		return "", args.Error(1)
	}
	return args.Get(0).(string), nil
}

func (m *MockUsecases) FindOne(ctx context.Context, userID string) (*user.User, error) {
	args := m.Called(userID)
	if args.Get(1) != nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*user.User), nil
}

func (m *MockUsecases) List(ctx context.Context, cond *user.Cond, limit, offset int) ([]*user.User, error) {
	args := m.Called(cond)
	if args.Get(1) != nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]*user.User), nil
}

func (m *MockUsecases) Update(ctx context.Context, in *user.UpdateInput) (*user.User, error) {
	args := m.Called(in)
	if args.Get(1) != nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*user.User), nil
}
