package usecases

import (
	"context"
	"gitlab.com/xamops/userService/internal/domain/user"
)

type UserUpdater interface {
	Update(ctx context.Context, in *user.UpdateInput) (*user.User, error)
}

type userUpdater struct {
	userRepo user.Repository
}

func NewUserCUpdater(userRepo user.Repository) UserUpdater {
	return &userUpdater{
		userRepo: userRepo,
	}
}

func (uu *userUpdater) Update(ctx context.Context, in *user.UpdateInput) (*user.User, error) {
	u, err := uu.userRepo.FindOne(ctx, in.ID)
	if err != nil {
		return nil, err
	}
	if err := u.Update(in); err != nil {
		return nil, err
	}
	if err := uu.userRepo.Update(ctx, u); err != nil {
		return nil, err
	}
	return u, nil
}
