package usecases

import (
	"context"
	"gitlab.com/xamops/userService/internal/domain/user"
)

type UserReader interface {
	FindOne(ctx context.Context, userID string) (*user.User, error)
	List(ctx context.Context, cond *user.Cond, limit, offset int) ([]*user.User, error)
}

type userReader struct {
	userRepo user.Repository
}

func NewUserReader(userRepo user.Repository) UserReader {
	return &userReader{
		userRepo: userRepo,
	}
}

func (ur *userReader) FindOne(ctx context.Context, userID string) (*user.User, error) {
	return ur.userRepo.FindOne(ctx, userID)
}

func (ur *userReader) List(ctx context.Context, cond *user.Cond, limit, offset int) ([]*user.User, error) {
	return ur.userRepo.Find(ctx, cond, 0, 0)
}
