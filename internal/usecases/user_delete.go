package usecases

import (
	"context"
	"gitlab.com/xamops/userService/internal/domain/user"
)

type UserDeleter interface {
	Delete(ctx context.Context, userID string) (string, error)
}

type userDeleter struct {
	userRepo user.Repository
}

func NewUserDeleter(userRepo user.Repository) UserDeleter {
	return &userDeleter{
		userRepo: userRepo,
	}
}

func (ud *userDeleter) Delete(ctx context.Context, userID string) (string, error) {
	usr, err := ud.userRepo.FindOne(ctx, userID)
	if err != nil {
		return "", err
	}
	usr.Delete()
	if err := ud.userRepo.Update(ctx, usr); err != nil {
		return "", err
	}
	return usr.ID(), nil
}
