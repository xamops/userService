package validator

import "github.com/xamust/xvalidator"

func NewValidator() (xvalidator.XValidator, error) {
	return xvalidator.NewXValidator()
}
