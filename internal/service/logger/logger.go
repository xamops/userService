package logger

import (
	"log/slog"
	"os"
)

const (
	developEnv = "dev"
	prodEnv    = "prod"

	op = "operation"
)

func NewLogger(env string) *slog.Logger {
	var handler *slog.JSONHandler
	switch env {
	case developEnv:
		handler = slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug})
	case prodEnv:
		handler = slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo})
	}
	return slog.New(handler)
}

func AddOperation(in string) slog.Attr {
	return slog.String(op, in)
}
