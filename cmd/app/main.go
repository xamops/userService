package main

import (
	"gitlab.com/xamops/userService/internal/di"
	"gitlab.com/xamops/userService/internal/service/logger"
	"log/slog"
)

func main() {
	// need config, maybe viper... (now use env)
	appLevel := di.MustEnv("APP_STAGE")
	// need logger, maybe slog
	log := logger.NewLogger(appLevel)
	log.Info("Starting application...", slog.String("app stage", appLevel))
	// app init

	// app run
}
