package main

import (
	"gitlab.com/xamops/userService/internal/di"
	"gitlab.com/xamops/userService/internal/service/migrator"
	"os"
)

/*
go run migrator status
go run migrator create filename sql
go run migrator up
go run migrator down
*/

func main() {
	migr := migrator.NewGooseMigrator(di.Database())
	if err := migr.Commands(os.Args[1], os.Args[2:]...); err != nil {
		panic(err)
	}
}
