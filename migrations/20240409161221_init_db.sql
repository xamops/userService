-- +goose Up
-- +goose StatementBegin
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create function update_updated_at()
    returns trigger as $$
begin
    new.updated_at = now();
    return new;
end;
$$ language 'plpgsql';

create function create_user_cabinets()
    returns trigger as $$
    declare
        sur_user_id uuid = new.id;
    begin
        insert into user_cabinets (user_id) values (sur_user_id);
        RETURN NULL;
    end;
$$ language 'plpgsql';

create function delete_user_cabinets()
    returns trigger as $$

declare
    main_user_id uuid = new.id;
    user_deleted_at timestamptz = new.deleted_at;
begin
    if (user_deleted_at is not null) then
        update user_cabinets set deleted_at = user_deleted_at where user_id = main_user_id;
    end if;
    RETURN NULL;
end;
$$ language 'plpgsql';

create table users(
                         id uuid default uuid_generate_v4() not null,
                         auth_id uuid not null,
                         login varchar(128) not null,
                         is_active bool default true,
                         status varchar(40) not null,
                         created_at timestamptz default now(),
                         updated_at timestamptz default now(),
                         deleted_at timestamptz
);
create trigger update_users_updated_at
    before UPDATE
    on
        users
    for each row
execute procedure update_updated_at();

create trigger create_user_cabinets
    after INSERT
    on
        users
    for each row
execute procedure create_user_cabinets();

create trigger delete_user_cabinets
    after UPDATE
    on
        users
    for each row
execute procedure delete_user_cabinets();

create table user_cabinets(
                              id uuid default uuid_generate_v4() not null,
                              user_id uuid not null,
                              name varchar(255) not null,
                              email varchar(255) not null,
                              description varchar(255) not null,
                              role varchar(20) not null,
                              metadata    jsonb       not null,
                              created_at timestamptz default now(),
                              updated_at timestamptz default now(),
                              deleted_at timestamptz
);
create trigger update_user_cabinets_updated_at
    before update
    on
        user_cabinets
    for each row
execute procedure update_updated_at();

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin


drop table users cascade ;
drop table user_cabinets cascade ;
drop function update_updated_at() cascade;
drop function create_user_cabinets() cascade;
drop function delete_user_cabinets() cascade;

-- +goose StatementEnd
